let variavelA = 'A'; // B
let variavelB = 'B'; // C
let variavelC = 'C'; // A

// solucão 1
// const temporaria = variavelA;

// variavelA = variavelB;
// variavelB = variavelC;
// variavelC = temporaria;

// console.log(variavelA, variavelB, variavelC);

// solucao 2
[variavelA, variavelB, variavelC] = [variavelB, variavelC, variavelA];

console.log(variavelA, variavelB, variavelC);