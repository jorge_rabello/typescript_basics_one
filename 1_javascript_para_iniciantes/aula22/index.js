// primitivos imutáveis - string, number, boolean, undefined null, bigint, symbol
let nome = 'Jorge';
nome = 'Rabello';
console.log(nome);

nome[0] = 'X';
console.log(nome[0], nome);

let a = 'A';
let b = a; // cópia
console.log(a, b);

a = 'Outra coisa';
console.log(a, b);

// referência - mutável - array, object, function
let x = [1, 2, 3];
let y = x; // x e y apontam para a mesma referência (endereço) na memória
console.log(x, y);

x.push(4); // ao alterar x tmbm alteramos y e vice-versa
console.log(x, y);

y.pop()
console.log(x, y); // ao alterar x tmbm alteramos y e vice-versa

const pessoa1 = {
  nome: 'Jorge',
  sobrenome: 'Rabello'
};

console.log(pessoa1);

const pessoa2 = pessoa1;

pessoa1.nome = 'Alexandre';

console.log(pessoa2);

pessoa2.sobrenome = 'Teste';

console.log(pessoa2);