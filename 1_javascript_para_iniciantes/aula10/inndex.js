// Tipos de Dados Primitivos

// string
const nome = 'Luiz';
const nome1 = "Luiz";
const nome2 = `Luiz`;

// number
const num1 = 10;
const num2 = 10.52;

// undefined -> não aponta pra um local na memória
let nomeAluno;

// Nulo -> não aponta pra um local na memória
const sobreNomeAluno = null;

// pode ser configurada como true ou false
const aprovado = true;

console.log(typeof nome);
console.log(typeof num1);
console.log(typeof nomeAluno);
console.log(typeof sobreNomeAluno);
console.log(typeof aprovado);

const a = [1, 2];
const b = a;

console.log(a, b); // [1,2] [1,2]

b.push(3);

/**
 * apesar de se ter modificado apenas b, a e b apontam para o mesmo local na memória
 * por isso a alteracão se refletiu nos 2
* */
console.log(a, b);  // [1,2, 3] [1,2, 3]