let umaString = "Um texto";
let umaFrase = "O rato roeu a rola do rei de roma.";

console.log(umaString.charAt(8));

console.log(umaString.concat(' em um teste !'));

console.log(umaString.indexOf('texto'));
console.log(umaString.indexOf('o', 3));

console.log(umaString.lastIndexOf('t'));

console.log(umaString.match(/[a-z]/g));
console.log(umaString.search(/[a-z]/g));
console.log(umaString.replace('Um', 'Outro'));
console.log(umaFrase.replace(/r/g, '#'));
console.log(umaFrase.length);
console.log(umaFrase.slice(2, 6));
console.log(umaFrase.slice(-5));
console.log(umaFrase.slice(-5, -1));
console.log(umaFrase.substring(umaFrase.length -5));
console.log(umaFrase.substring(umaFrase.length -5, umaFrase.length -1));

console.log(umaFrase.split(' '));
console.log(umaFrase.split(' ', 2));
console.log(umaFrase.toUpperCase());
console.log(umaFrase.toLocaleLowerCase());