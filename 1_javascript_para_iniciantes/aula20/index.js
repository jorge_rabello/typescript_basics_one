function saudacao(nome) {
  console.log(`Bom dia ${nome} !`);
}

function soma(n1, n2) {
  return  Number(n1) + Number(n2);
}

function multiplica(n1 = 1, n2 = 10) {
  return Number(n1) * Number(n2);
}

saudacao('Jorge');
saudacao('Maria');
saudacao('Duda');

const resultado =  soma(10, 2);
const resultado2 =  soma('10', '2');
const resultado3 =  soma('10', 2);
const resultado4 =  multiplica(2, 2);
const resultado5 =  multiplica();

console.log(resultado);
console.log(resultado2);
console.log(resultado3);
console.log(resultado4);
console.log(resultado5);

// funcoes anonimas
const raiz = function(n) {
  return Math.sqrt(n);
}

// arrow function
// const raizArrow = (n) => {
//   return Math.sqrt(n);
// }

// arrow function mais simples
const raizArrow = n => Math.sqrt(n);


console.log(raiz(9));
console.log(raizArrow(9));