// Operadores

// + adicão
const number1 = 5;
const number2 = 10;
console.log(number1 + number2); // Adicão
console.log(number1 - number2); // Subtracão
console.log(number1 * number2); // Multiplicacão
console.log(number1 / number2); // Divisão
console.log(number1 ** number2); // Potenciacão
console.log('p', number1 % number2); // Módulo - Resto da divisão

// precedencia
console.log(number1 + number2 * number1); // 55
console.log((number1 + number2) * number1); // 75

// concatenacão
const n1 = '5';
const n2 = '10';
console.log(n1 + n2);

// pós incremento
let contador = 1;

contador++;
console.log(contador);

contador--;
console.log(contador);

contador += 5;
console.log(contador);

contador -= 2;
console.log(contador);

contador *= 2;
console.log(contador);

contador /= 2;
console.log(contador);

// pós e pré incremento
let contador2 = 1;
console.log(contador2++); // 1 por que exibe primeiro e incrementa depois, sendo assim a partir daqui contador2 = 2
console.log(++contador2); // 3 2 + 1 = 3 - nesse caso incrementa primeiro e depois exibe

const num1 = 10;
const num2 = parseInt('5')
const num3 = parseFloat('5.2');
const num4 = Number('5');
console.log(num1 + num2);
