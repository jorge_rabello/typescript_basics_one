
// var permite que a variável seja mutada caso seja redeclarada
var nome = 'Jorge';
var nome = 'Rabello';
console.log(nome);


// let faz com que não seja possível redeclarar a variável
let nickName = 'aNickName';
// let nickName = 'anotherNickName'; aqui ocorre um erro
console.log(nickName);