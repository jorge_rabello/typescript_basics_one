// objeto
const pessoa1 = {
  nome: 'Jorge',
  sobrenome: 'Rabello',
  idade: 25
};

console.log(pessoa1.nome);
console.log(pessoa1.sobrenome);
console.log(pessoa1.idade);

// factory function
function criaPessoa(nome, sobrenome, idade) {
  return {
    nome: nome,
    sobrenome: sobrenome,
    idade: idade
  };
}

function criaPessoaV2(nome, sobrenome, idade) {
  return { nome, sobrenome, idade };
}

const maria = criaPessoa('Maria', 'Rabello', 33);
console.log();
console.log(maria.nome);

const joao = criaPessoaV2('João', 'Rabello', 33);
console.log();
console.log(joao.nome);

const novaPessoa = {
  nome: 'Luiz',
  sobrenome: 'Miranda',
  idade: 25,

  fala() {
    console.log(`${this.nome} está falando oi !!!`);
  },

  fezAniversario() {
    this.idade++;
  }
}

novaPessoa.fala();
console.log(novaPessoa.idade);
novaPessoa.fezAniversario();
console.log(novaPessoa.idade);