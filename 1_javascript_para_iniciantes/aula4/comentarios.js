// Comentários inline
console.log('Olá mundo !'); // aqui tem outro comentário
console.log('Linha 1');

/*
Esse é um comentário de bloco
para o caso de um textão ;D
*/
console.log('Linha 2');
console.log('Linha 3');

/**
 * Comentários de bloco também
 * podem ser escritos assim
 * com esse '*'
 * * */

console.log('Linha 4');
// Os comentários são ignorados !

// console.log('Código comentado')

/**
 * console.log('Código comentado em bloc')
 ***/