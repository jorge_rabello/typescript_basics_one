let num1 = 9.54578;

console.log(Math.floor(num1));
console.log(Math.ceil(num1));
console.log(Math.round(num1));
console.log(Math.max(1, 2, 3, 4, 5, 6, -10, -50, 1500, 9, 8, 7, 6));
console.log(Math.min(1, 2, 3, 4, 5, 6, -10, -50, 1500, 9, 8, 7, 6));
console.log(Math.random()); // a pseudo-random number between 0 and 1

const aleatorio = Math.random() * (10 - 5) + 5; // a pseudo-random number between 0 and 5
console.log(aleatorio);

console.log(Math.PI);
console.log(Math.pow(2, 10));
console.log(2 ** 10);

console.log(Math.sqrt(9));
console.log(9 ** (1 / 2));
console.log(9 ** 0.5);

console.log(10 / 0.000000000000001);
console.log(10 / 0.000000000000000000000000001);
console.log(10 / 0); // Infinity
