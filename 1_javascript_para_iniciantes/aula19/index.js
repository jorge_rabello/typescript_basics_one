// apesar de aceitar dados hetergêneos const alunos = ['Luiz',  'Maria', 'João', 1, true, null]; não é recomendável
const alunos = ['Luiz', 'Maria', 'João'];

console.log(alunos);

console.log(alunos[0]); // Luiz
console.log(alunos[1]); // Maria
console.log(alunos[2]); // João
console.log(alunos[4]); // undefined

alunos[0] = 'Eduardo'
console.log();
console.log(alunos[0]); // Eduardo
console.log(alunos[1]); // Maria
console.log(alunos[2]); // João
console.log(alunos[4]); // undefined

console.log(alunos);

alunos[3] = 'Luiza';
console.log(alunos);

console.log(alunos.length);

alunos[alunos.length] = 'Fabio';
alunos[alunos.length] = 'Luana';
alunos[alunos.length] = 'Pedro';

console.log(alunos);

alunos.push('Jorge');
alunos.push('Ana');

console.log(alunos);

alunos.unshift('Mauro');
alunos.unshift('Bianca');

console.log(alunos);

alunos.pop();

console.log(alunos);

const removido = alunos.pop();
console.log(removido);
console.log(alunos);

const removidoDoInicio = alunos.shift()
console.log(removidoDoInicio);
console.log(alunos);

delete alunos[1];
console.log(alunos); // <1 empty item>
console.log(alunos[1]); // undefined

alunos[1] = 'Senna';

console.log(alunos.slice(0, 3));
console.log(alunos.slice(0, -3));

console.log(typeof alunos); // object
console.log(alunos instanceof Array); // true

const array = [1,2,3,4,5];
array.pop();
array[0] = 1024;
console.log(array); // [ 1024, 2, 3, 4 ]

const array2 = [1,2,3,4,5];
array2 = 'Legal';  // TypeError: Assignment to constant variable.

