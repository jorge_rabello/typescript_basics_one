/**
 * Luiz Otávio Miranda tem 30 anos, pesa 84 Kg
 * tem 1.8 de altura e seu IMC é de 25.989829829282982928928
 * Luiz Otávio nasceu em 1980
 * * */

const nome = 'Jorge';
const sobrenome = 'Rabello';
const idade = 33;
const peso = 120;
const altura = 1.68;
const anoAtual = 2021;

let imc = peso / Math.pow(altura, 2);
let anoNascimento = anoAtual - idade;

console.log(`${nome} ${sobrenome} tem ${idade} anos, e pesa ${peso} Kg`);
console.log(`tem ${altura} metros de altura e seu IMC é de ${imc}`);
console.log(`${nome} nasceu em ${anoNascimento}`);
